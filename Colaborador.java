
import java.util.ArrayList;

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Colaborador extends Usuario{
    private ArrayList<Boleto> boletos;
    private int boletosVendidos;
    private static int cantidadBoletos;
    public Comprador compradores [];
    public int actualCompradores;
    
    

    public Colaborador(String nombre, int id, String telefono, String direccion, int cp, String ciudad, String estado,ArrayList<Boleto> boletos) {   
        super(nombre, id, telefono, direccion, cp, ciudad, estado);
        this.boletos = boletos;
        compradores = new Comprador[boletos.size()];
    }
    
    
    public void AsignarBoleto(int indice,Comprador comprador){
        if(boletos.get(indice).getEstado() == false){
            boletos.get(indice).setEstado(true);
            comprador.setBoleto(boletos.get(indice));
            boletosVendidos++;
        }
    }
    
    public void CrearComprador(String nombre, int id, String telefono, String direccion, int cp, String ciudad, String estado){
        Comprador c1 = new Comprador(nombre, id, telefono,direccion,cp,ciudad,estado);
        compradores[actualCompradores] = c1;
        actualCompradores++;
        
        
    }

    public int getBoletosVendidos() {
        return boletosVendidos;
    }

    
    
    
   
    
   
}
