
/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Comprador extends Usuario{
    private Boleto boleto;

    public Comprador(String nombre, int id, String telefono, String direccion, int cp, String ciudad, String estado) {
        super(nombre, id, telefono, direccion, cp, ciudad, estado);
    }

    public Boleto getBoleto() {
        return boleto;
    }

    public void setBoleto(Boleto boleto) {
        this.boleto = boleto;
    }
    
}
