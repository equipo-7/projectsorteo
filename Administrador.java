
import java.util.ArrayList;

/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Administrador extends Usuario{
    private Colaborador colaboradores [];
    public static Comprador compradores [];
    public static int ActualCompradores;
    private ArrayList <Boleto> boletos = new ArrayList<Boleto>();
    private int noActualColaboradores;
    private int noActualBoletos;
    

    public Administrador(String nombre, int id, String telefono, String direccion, int cp, String ciudad, String estado,int noBoletos,int maxColaboradores,int costo) {
        super(nombre, id, telefono, direccion, cp, ciudad, estado);         
        compradores = new Comprador[noBoletos];
        colaboradores = new Colaborador[maxColaboradores];  
        for(int i = 0; i<noBoletos;i++){
            boletos.add(new Boleto(i,costo,false));
        }
    }
    
    
    public void crearColaborador(String nombre, int id,String noTelefono,String direccion,int cp,String ciudad,String estado){
         Colaborador c1 = new Colaborador(nombre,id,noTelefono,direccion,cp,ciudad,estado,boletos);
         colaboradores[noActualColaboradores] = c1;
         noActualColaboradores++;
    }
    
    
    public void invalidarSorteo(){
        colaboradores =null;
        boletos = null;
        System.out.println("El sorteo se ha cancelado");
    }
    

}
