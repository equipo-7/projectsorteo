
import java.util.ArrayList;
import java.util.Scanner;



/**
 *
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Menu {
    ArrayList <Administrador> sorteos = new ArrayList<>();
    Scanner s = new Scanner(System.in);
    public static void main(String[] args){
        
        Menu m = new Menu();
        int opcion = -1;
        do{
           
            System.out.println("0.-Salir");
            System.out.println("1.-Crear Sorteo.");
            System.out.println("2.-Modo Administrador");
            System.out.println("3.-Modo Vendedor");
            opcion = m.s.nextInt();
            
            
            //casos.
            switch(opcion){
                case 1:
                    m.crearSorteo();
                    break;
                case 2:
                    break;
                case 3:
                    break;               
            }
                                 
        }while(opcion!=0);
        
        
     
    }
    
    private void crearSorteo(){
        System.out.println("Ingresa el nombre del Administrador: ");
        String nombre = s.nextLine();
        System.out.println("Ingresa el id: ");
        int id = s.nextInt();
        System.out.println("Ingresa el telefono:");
        String telefono = s.nextLine();
        System.out.println("Ingresa la direccion");
        String direccion = s.nextLine();
        System.out.println("Ingresa el codigo postal");
        int cp = s.nextInt();
        System.out.println("Ingresa la ciudad");
        String ciudad = s.nextLine();
        System.out.println("Ingresa el estado");
        String estado = s.nextLine();
        System.out.println("Ingresa el numero de Boletos");
        int noBoletos = s.nextInt();
        System.out.println("Ingresa el numero maximo de colaboradores");
        int maxColaboradores = s.nextInt();
        System.out.println("Ingresa el costo del boleto");
        int costoBoleto = s.nextInt();
        Administrador a = new Administrador(nombre,id,telefono,direccion,cp,ciudad,estado,noBoletos,maxColaboradores,        costoBoleto);
        sorteos.add(a);
    }
}

